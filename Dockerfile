FROM node:14.16.1-alpine3.10 AS builder

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build --prod

FROM node:14.16.1-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

COPY --from=builder /usr/src/app/dist/ dist/

CMD ["node", "dist/main.js"]
