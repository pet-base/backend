import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePetDto } from './dto/create-pet.dto';
import { UpdatePetDto } from './dto/update-pet.dto';
import { Pet } from './entities/pet.entity';

@Injectable()
export class PetService {

  constructor(@InjectRepository(Pet) private petRepository: Repository<Pet>) {}

  create(createPetDto: CreatePetDto) {
    return this.petRepository.save(createPetDto);
  }

  findAll() {
    return this.petRepository.find();
  }

  findOne(id: number) {
    return this.petRepository.findOne(id);
  }

  update(id: number, updatePetDto: UpdatePetDto) {
    return this.petRepository.update(id, updatePetDto);
  }

  remove(id: number) {
    return this.petRepository.delete(id);
  }
}
